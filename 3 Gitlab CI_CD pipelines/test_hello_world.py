import pytest
import os
import tempfile
import hello_world


@pytest.fixture
def client():
    db_fd, hello_world.app.config['DATABASE'] = tempfile.mkstemp()
    hello_world.app.config['TESTING'] = True

    with hello_world.app.test_client() as client:
        yield client

    os.close(db_fd)


def test_hello_world(client):
    rv = client.get('/')
    assert b'hello world' == rv.data
