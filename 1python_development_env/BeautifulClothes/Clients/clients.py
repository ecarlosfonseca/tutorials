import pandas as pd
from datetime import datetime


class Client:

    current_year = int(datetime.now().year)

    def __init__(self, first_name: str,
                 last_name: str, age: int, gender: str, profession: str,
                 height: float, weight: float, value_spent: float):

        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.gender = gender
        self.profession = profession
        self.height = height
        self.weight = weight
        self.year_of_birth = self.current_year - self.age
        self.value_spent = value_spent
        self.raw_data = pd.DataFrame(
            columns=['first_name', 'last_name', 'age', 'gender',
                     'profession', 'height', 'weight', 'year_of_birth'
                     'evaluation', 'value_spent']
        )

    def add_to_data(self, client) -> pd.DataFrame():
        """
        Adds the new created Clients df to the raw data df

        Parameters:
        Clients (list): a list with the Clients the new Clients inserted
        Returns:
        self.raw data (pd.DataFrame): raw data table with data expecting
        treatment updated with new Clients
        """
        self.raw_data = self.raw_data.append(client)
        return self.raw_data

    @staticmethod
    def gets_store_data(self):
        """
        Connects to the sql database and Stores today's entries
        Arguments: None
        Returns: None
        """
        connection = pymysql.connect(host='localhost',
                                     user='root',
                                     password='12345',
                                     db='employee')

        cursor = connection.cursor()

        cols = "`,`".join([str(i) for i in self.raw_data.columns.tolist()])

        for i, row in self.raw_data.iterrows():
            sql = "INSERT INTO `book_details` (`" + cols + "`) " \
                   "VALUES (" + "%s," * (len(row) - 1) + "%s)"
            cursor.execute(sql, tuple(row))

            connection.commit()

        connection.close()

    @staticmethod
    def generate_report(self, store: int):
        """
        Makes the connection to the sql database and queries the lines related
        to the store of interest, cleans and performs the intended analysis
        and creates a report
        Arguments (int): The store id to analyse
        Returns: Report
        """
        connection = pymysql.connect(host='localhost',
                                     user='root',
                                     password='12345',
                                     db='employee')
        my_cursor = connection.cursor()
        my_cursor.execute("SELECT * from employee")
        result = my_cursor.fetchall()
        report = 'file with the analysis'
