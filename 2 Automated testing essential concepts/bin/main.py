import sys

sys.path.append('2 Automated testing essential concepts/')

from utils.preprocess_data import GetResults


def run_get_results(path='../data',
                    file='test_scores2.csv',
                    objective='students_analysis'):

    result = GetResults(path, file, objective)

    files = result.get_files()

    df = files.process_files()

    df.create_file()

