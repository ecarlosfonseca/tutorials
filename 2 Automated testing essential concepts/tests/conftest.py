import pandas as pd
import pytest
import sys

sys.path.append('2 Automated testing essential concepts/')

from utils.preprocess_data import GetResults


@pytest.fixture
def raw_test_scores2():
    return pd.read_csv('../data/test_results2.csv')


@pytest.fixture
def get_files():
    result = GetResults(path='../data',
                        file='test_scores2.csv',
                        objective='students_analysis')
    return result.get_files()


@pytest.fixture
def results_scores2():
    result = GetResults(path='../data',
                        file='test_scores2.csv',
                        objective='students_analysis')
    return result.process_files()


@pytest.fixture
def results_scores2_wrong():
    result = GetResults(path='../data',
                        file='test_scores2_wrong.csv',
                        objective='students_analysis')
    return result.process_files()
