
def test_get_files(self, get_files):
    """
    Verifies output of get_files or the following function will not
    be able to run
    """
    assert len(get_files) != 0


def test_get_results(raw_test_scores2, results_test_scores2):
    """
    Verifies output of get_results so we do not save empty files on the
    following function
    """
    students_analysis, schools_analysis = results_test_scores2

    assert students_analysis.shape != (0, 0)
    assert schools_analysis.name.unique() == (0, 0)
