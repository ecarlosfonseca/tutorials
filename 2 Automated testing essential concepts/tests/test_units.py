import pytest


def test_get_results_quality(results_scores2, raw_test_scores2,
                             results_scores2_wrong):
    """
    Verifies output of get_results so we do not save empty files on the
    following function
    """

    students_analysis, schools_analysis = results_scores2

    assert students_analysis.name.unique() == raw_test_scores2.name.unique()
    assert schools_analysis.name.unique() == raw_test_scores2.school.unique()
    assert results_scores2_wrong == pytest.raises(ValueError)

