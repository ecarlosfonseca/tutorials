import pandas as pd
import numpy as np
from os import listdir
from os.path import isfile, join
from datetime import datetime


class GetResults:

    def __init__(self, path, file, objective):
        self.students_analysis = pd.DataFrame()
        self.schools_analysis = pd.DataFrame()
        self.files = list()
        self.file = file
        self.path = path
        self.objective = objective

    def get_files(self):
        """
        Filters the good files and enumerates them
        :param:
        path(str): path where the files are located
        :return:
        files(list): list with the files correct files in the directory
        """

        files = (
            [f for f in listdir(self.path) if isfile(join(self.path, f))])

        for file in files:
            df = pd.read_csv(self.path + file)
            if df.shape[1] == 4:
                self.files.append(file)

        return self.files

    def process_files(self):
        """
        Elaborates the passing/failing analysis of the requested file
        :param:
        file(str): The name of the file to process
        :return:
        students_analysis(pd.DataFrame): Students analysis from the
        inputted file
        schools_analysis(pd.DataFrame): Schools analysis from the
        inputted file
        """

        if self.file not in self.files:
            raise ValueError('This file either does not exist or'
                             'it is a wrong duplicate file')

        date = datetime.now()
        date = pd.to_datetime(date, format='%d/%m/%Y')

        df = pd.read_csv(self.path+self.file, sep=';')
        if df.shape[1] == 4:
            df['username'] = df.name.astype(str) \
                             + '@' \
                             + df.school.str.replace(' ', '')
            df['birth'] = pd.to_datetime(df.birth, format='%d/%m/%Y')
            df['age'] = df.birth.apply(lambda x: round((date - x)
                                                       .days/365.25))
            df['index'] = 1 - ((df.age - 10) / 10)
            df['score'] = df['correct_answers'] * df['index']
            df['result'] = np.where(
                df.score >= 30,
                'Passed',
                'Failed'
            )
            analysis_schools = df.groupby(['school']).mean()['score']\
                .reset_index()
            analysis_schools['result'] = np.where(
                analysis_schools.score >= 30,
                'Passed',
                'Failed'
            )
            self.students_analysis = df[['name', 'score', 'result']],
            self.schools_analysis = analysis_schools
            if self.objective == 'students_analysis':
                return self.students_analysis
            elif self.objective == 'schools_analysis':
                return self.schools_analysis
            else:
                raise ValueError('Objective not defined')

    def create_file(self):
        """
        Creates csv files with the passing/failing analysis from both
        students and schools
        :return: None
        """
        if self.objective == 'students_analysis':
            self.students_analysis.to_csv(self.path
                                          + '../results/'
                                          + self.file
                                          + '_'
                                          + self.objective
                                          + '.csv')
        elif self.objective == 'schools_analysis':
            self.schools_analysis.to_csv(self.path
                                         + '../results/'
                                         + self.file
                                         + '_'
                                         + self.objective
                                         + '.csv')

